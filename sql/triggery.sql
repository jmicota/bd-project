--BD PROJEKT MODEL LOGICZNY--
--TRIGGERY--

--Justyna Micota 418427--
--Paweł Sarzyński 418407--

create or replace trigger datarej
before insert on uzytkownik
for each row
BEGIN
    SELECT sysdate INTO :NEW.data_rejestracji FROM dual;
END;
/

create or replace trigger czasbanu
before insert on ban
for each row
DECLARE
    ile number;
BEGIN
    ile := :NEW.liczba_dni;
    SELECT current_timestamp INTO :NEW.data_zbanowania FROM dual;
    SELECT current_timestamp + ile INTO :NEW.data_wygasniecia FROM dual;
END;
/

create or replace trigger datadot
before insert on dotacja
for each row
BEGIN
    SELECT current_timestamp INTO :NEW.data_wplaty FROM dual;
END;
/

create or replace trigger sprban
before insert on ban
for each row
DECLARE
    r_a number;
    p_u number;
BEGIN
    select admin.ranga_admina into r_a from admin where admin.id_uzytkownika = :NEW.id_admina;
    select uzytkownik.nr_poziomu_uzytkownika into p_u from uzytkownik where uzytkownik.id_uzytkownika = :NEW.id_uzytkownika;
    IF (r_a < p_u) THEN
        raise_application_error(-20000, 'Ranga admina niewystarczajaca do zbanowania tego uzytkownika!');
    END IF;
END;
/