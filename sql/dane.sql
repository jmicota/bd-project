--BD PROJEKT MODEL LOGICZNY--
--DANE PRZYKLADOWE--

--Justyna Micota 418427--
--Paweł Sarzyński 418407--

insert into poziom_uzytkownika(nazwa_poziomu, kolor, min_wiek, wymag_liczba_rozeg, wymag_liczba_wygr)
values ('Amator', '#1475a5', 8, 0, 0);
insert into poziom_uzytkownika(nazwa_poziomu, kolor, min_wiek, wymag_liczba_rozeg, wymag_liczba_wygr)
values ('Zawodowiec', '#003882', 8, 100, 100);
insert into poziom_uzytkownika(nazwa_poziomu, kolor, min_wiek, wymag_liczba_rozeg, wymag_liczba_wygr)
values ('Premium', '#dfd805', 8, 0, 0);
insert into poziom_uzytkownika(nazwa_poziomu, kolor, min_wiek, wymag_liczba_rozeg, wymag_liczba_wygr)
values ('Admin', '#af1818', 18, 0, 0);

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz1', 123, 1, 'Jan', 'Kowalski',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz2', 123, 4, 'Andrzej', 'Nowak',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz3', 123, 1, 'Jadwiga', 'Strzelecka',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz4', 123, 2, 'Stanislaw', 'Stalin',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz5', 123, 4, 'Adam', 'Morawiecki',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz6', 123, 3, 'Anna', 'Kowalczyk',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz7', 123, 4, 'Marcin', 'Dabrowski',
                               to_date('01/01/2001', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz8', 123, 1, 'Malgorzata', 'Polak',
                               to_date('01/01/2001', 'DD/MM/YY'),
                               to_date('01/01/2001', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz9', 123, 1, 'Michal', 'Pion',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz10', 123, 1, 'Jan', 'Strzelba',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz11', 123, 2, 'Karolina', 'Kondraciuk',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz12', 123, 1, 'Hubert', 'Podolski',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz13', 123, 1, 'Adam', 'Wieczorek',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz14', 123, 2, 'Kacper', 'Malaga',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz15', 123, 1, 'Michal', 'Kowalski',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

                               insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz16', 123, 1, 'Paulina', 'Jamnik',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

                               insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz17', 123, 2, 'Olga', 'Darno',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

                               insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz18', 123, 4, 'Filip', 'Torun',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

                               insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz19', 123, 3, 'Rafal', 'Hojny',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

                               insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia, data_rejestracji)
values ('gracz20', 123, 1, 'Michal', 'Precel',
                               to_date('01/01/2005', 'DD/MM/YY'),
                               to_date('01/01/2005', 'DD/MM/YY'));

insert into admin values (7, null, 4);
insert into admin values (5, 7, 3);
insert into admin values (2, 5, 2);
insert into admin values (18, 2, 1);

insert into ban(id_uzytkownika, id_admina, liczba_dni, data_zbanowania, data_wygasniecia)
values (1, 7, 1, timestamp '2020-05-10 14:00:00', timestamp '2020-05-11 14:00:00');
insert into ban(id_uzytkownika, id_admina, liczba_dni, data_zbanowania, data_wygasniecia)
values (13, 7, 5, timestamp '2020-05-10 14:00:00', timestamp '2020-05-15 14:00:00');
insert into ban(id_uzytkownika, id_admina, liczba_dni, data_zbanowania, data_wygasniecia)
values (10, 7, 10, timestamp '2021-01-10 14:00:00', timestamp '2021-03-10 14:00:00');
-- 5 admin
insert into ban(id_uzytkownika, id_admina, liczba_dni, data_zbanowania, data_wygasniecia)
values (15, 5, 10, timestamp '2021-05-10 14:00:00', timestamp '2021-05-15 14:00:00');
insert into ban(id_uzytkownika, id_admina, liczba_dni, data_zbanowania, data_wygasniecia)
values (16, 5, 10, timestamp '2021-05-10 14:00:00', timestamp '2021-05-15 14:00:00');
-- 2 admin
insert into ban(id_uzytkownika, id_admina, liczba_dni, data_zbanowania, data_wygasniecia)
values (17, 2, 10, timestamp '2021-05-10 14:00:00', timestamp '2021-05-15 14:00:00');

insert into dotacja(id_uzytkownika, kwota, data_wplaty) values (6, 100, timestamp '2020-06-10 14:00:00');
insert into dotacja(id_uzytkownika, kwota, data_wplaty) values (6, 10, timestamp '2020-06-10 14:00:00');
insert into dotacja(id_uzytkownika, kwota, data_wplaty) values (19, 50, timestamp '2020-06-10 14:00:00');
insert into dotacja(id_uzytkownika, kwota, data_wplaty) values (7, 5, timestamp '2020-06-10 14:00:00');

insert into opis(pelny_opis) values('Szachy to strategiczna gra planszowa rozgrywana przez dwoch graczy na 64-polowej szachownicy za pomoca zestawu bierek (pionow i figur).');
insert into opis(pelny_opis) values('Warcaby to strategiczna gra planszowa rozgrywana przez dwoch graczy na 64-polowej warcabnicy z wykorzystaniem 24 pionkow.');
insert into opis(pelny_opis) values('Chinczyk to gra planszowa przeznaczona dla 2, 3 lub 4 osob oparta na losowosci (rzutach kostka).');
insert into opis(pelny_opis) values('Mahjong to pochodzaca z Chin gra dla 4 graczy wykorzystujaca komplet 144 kamieni podzielonych na grupy podobnie do kolorow w kartach. Istnieja wersje zasad z mniejszym lub wiekszym znaczeniem losowosci.');
insert into opis(pelny_opis) values('Poker to gra karciana dla minimum 2 graczy rozgrywana najczesciej jedna talia 52 kart, celem jest wygranie pieniedzy lub zetonow od pozostalych graczy dzieki skompletowaniu najlepszego ukladu lub za pomoca blefu.');
insert into opis(pelny_opis) values('Pasjans to ukladanka karciana dla 1 osoby (zwana takze samotnikiem) ukladana przy uzyciu 1 lub 2 talii kart.');

insert into gra(nazwa_gry, id_opisu) values('szachy', 1);
insert into gra(nazwa_gry, id_opisu) values('warcaby', 2);
insert into gra(nazwa_gry, id_opisu) values('chinczyk', 3);
insert into gra(nazwa_gry, id_opisu) values('mahjong', 4);
insert into gra(nazwa_gry, id_opisu) values('poker', 5);
insert into gra(nazwa_gry, id_opisu) values('pasjans', 6);

insert into tryb_gry(nazwa_trybu, wymagany_poziom) values('treningowy', 1);
insert into tryb_gry(nazwa_trybu, wymagany_poziom) values('rankingowy', 2);
insert into tryb_gry(nazwa_trybu, wymagany_poziom) values('hazardowy', 3);

insert into znajomosc values(1, 2);
insert into znajomosc values(1, 3);
insert into znajomosc values(1, 4);
insert into znajomosc values(2, 5);
insert into znajomosc values(2, 6);
insert into znajomosc values(3, 4);
insert into znajomosc values(3, 5);
insert into znajomosc values(1, 5);
insert into znajomosc values(2, 4);
insert into znajomosc values(3, 6);

insert into trofeum(nazwa_trofeum, id_gry, id_trybu, wym_poziom, wym_liczba_zwyciestw, wym_proc_zwyciestw)
values('Szachista', 1, 2, 1, 5, 0);
insert into trofeum(nazwa_trofeum, id_gry, id_trybu, wym_poziom, wym_liczba_zwyciestw, wym_proc_zwyciestw)
values('Hazardowy Szachista', 1, 3, 1, 50, 90);
insert into trofeum(nazwa_trofeum, id_gry, id_trybu, wym_poziom, wym_liczba_zwyciestw, wym_proc_zwyciestw)
values('Krol Warcabow', 2, 1, 1, 25, 75);
insert into trofeum(nazwa_trofeum, id_gry, id_trybu, wym_poziom, wym_liczba_zwyciestw, wym_proc_zwyciestw)
values('Pokerface', 5, 2, 1, 25, 75);
insert into trofeum(nazwa_trofeum, id_gry, id_trybu, wym_poziom, wym_liczba_zwyciestw, wym_proc_zwyciestw)
values('Hazardowy Pokerzysta', 5, 3, 1, 25, 75);
insert into trofeum(nazwa_trofeum, id_gry, id_trybu, wym_poziom, wym_liczba_zwyciestw, wym_proc_zwyciestw)
values('Tytan Mahjonga', 4, 1, 1, 1000, 0);
insert into trofeum(nazwa_trofeum, id_gry, id_trybu, wym_poziom, wym_liczba_zwyciestw, wym_proc_zwyciestw)
values('Tytan Pasjansa', 6, 1, 1, 1000, 0);
insert into trofeum(nazwa_trofeum, id_gry, id_trybu, wym_poziom, wym_liczba_zwyciestw, wym_proc_zwyciestw)
values('Rodowity Chinczyk', 3, 1, 1, 25, 75);

insert into trofeum_uzytkownika values(1, 1);
insert into trofeum_uzytkownika values(2, 1);
insert into trofeum_uzytkownika values(3, 1);
insert into trofeum_uzytkownika values(2, 2);
insert into trofeum_uzytkownika values(1, 2);
insert into trofeum_uzytkownika values(4, 3);
insert into trofeum_uzytkownika values(2, 4);
insert into trofeum_uzytkownika values(1, 4);
insert into trofeum_uzytkownika values(2, 5);
insert into trofeum_uzytkownika values(4, 5);
insert into trofeum_uzytkownika values(3, 6);
insert into trofeum_uzytkownika values(2, 6);
insert into trofeum_uzytkownika values(2, 7);
insert into trofeum_uzytkownika values(1, 7);
insert into trofeum_uzytkownika values(1, 8);

insert into rozgrywka(id_gry, id_trybu) values (1, 1);
insert into rozgrywka(id_gry, id_trybu) values (1, 1);
insert into rozgrywka(id_gry, id_trybu) values (1, 1);
insert into rozgrywka(id_gry, id_trybu) values (1, 1);
insert into rozgrywka(id_gry, id_trybu) values (1, 1);
insert into rozgrywka(id_gry, id_trybu) values (1, 1);
insert into rozgrywka(id_gry, id_trybu) values (1, 1);
 
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values(1, 6, 1);
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values(1, 5, 2);
 
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values (2, 6, 1);
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values(2, 5, 2);
 
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values(3, 6, 1);
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values(3, 5, 2);
 
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values (4, 6, 1);
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values(4, 5, 2);
 
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values(5, 7, 1);
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values(5, 6, 2);
 
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values(6, 7, 1);
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values(6, 5, 2);
 
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values(7, 7, 1);
insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
values(7, 5, 2);

insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (1, 6, 'gracz6 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (1, 5, 'gracz5 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (1, 6, 'gracz6 wygral');

insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (2, 6, 'gracz6 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (2, 5, 'gracz5 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (2, 6, 'gracz6 wygral');

insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (3, 6, 'gracz6 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (3, 5, 'gracz5 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (3, 6, 'gracz6 wygral');

insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (4, 6, 'gracz6 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (4, 5, 'gracz5 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (4, 6, 'gracz6 wygral');

insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (5, 6, 'gracz6 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (5, 5, 'gracz5 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (5, 6, 'gracz6 wygral');

insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (6, 6, 'gracz6 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (6, 5, 'gracz5 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (6, 6, 'gracz6 wygral');

insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (7, 6, 'gracz6 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (7, 5, 'gracz5 ruszyl');
insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
values (7, 6, 'gracz6 wygral');