--BD PROJEKT MODEL LOGICZNY--
--ZAPYTANIA--

--Justyna Micota 418427--
--Paweł Sarzyński 418407--

select ID_TROFEUM, nazwa_trofeum from trofeum;
 
-- tutaj sa pokazane przykladowe rankingi, a nizej takze zapytania z
-- przykladowymi danymi. rankingi beda moglby byc robione takze z jednej
-- gry, albo z jednej gry i konkretnego trybu, jednak takie zapytania beda
-- generowane juz na podstawie wyborow uzytkownika w warstwie aplikacji, a tutaj
-- pokazane sa tylko szkielety.
 
-- ranking po procencie wygranych
with rozegrane as (
    select u.ID_UZYTKOWNIKA, count(distinct ID_ROZGRYWKI) ile
    from uzytkownik u
    left join wynik
    on u.ID_UZYTKOWNIKA = wynik.id_uzytkownika
    group by u.ID_UZYTKOWNIKA
),
wygrane as (
    select u.ID_UZYTKOWNIKA, count(distinct ID_ROZGRYWKI) ile
    from uzytkownik u
    left join wynik
    on u.ID_UZYTKOWNIKA = wynik.id_uzytkownika
    and POZYCJA = 1
    group by u.ID_UZYTKOWNIKA
)
select zap.ID_UZYTKOWNIKA,
       zap.proc,
       rank() over (order by proc desc) as pozycja
from (select wygrane.ID_UZYTKOWNIKA, case
    when rozegrane.ile = 0 then 0
    else wygrane.ile / rozegrane.ile
end proc
from rozegrane
left join wygrane
on rozegrane.ID_UZYTKOWNIKA = wygrane.ID_UZYTKOWNIKA) zap;
 
-- pozniej bedzie mozna uzyc pozywzszych tabel with as takze tutaj
-- ranking po ilosci wygranych
select zap.ID_UZYTKOWNIKA,
       ile,
       rank() over (order by ile desc) as pozycja
from (select u.ID_UZYTKOWNIKA, count(distinct ID_ROZGRYWKI) ile
from uzytkownik u
left join wynik
on u.ID_UZYTKOWNIKA = wynik.id_uzytkownika
and POZYCJA = 1
group by u.ID_UZYTKOWNIKA) zap;
 
-- ranking po ilosci rozegranych gier
select zap.ID_UZYTKOWNIKA,
       ile,
       rank() over (order by ile desc) as pozycja
from (select u.ID_UZYTKOWNIKA, count(distinct ID_ROZGRYWKI) ile
from uzytkownik u
left join wynik
on u.ID_UZYTKOWNIKA = wynik.id_uzytkownika
group by u.ID_UZYTKOWNIKA) zap;
 
-- ranking po ilosci trofeow
select zap.ID_UZYTKOWNIKA as id,
       ile,
       rank() over (order by ile desc) as pozycja
from (select u.ID_UZYTKOWNIKA, count(ID_TROFEUM) ile
from uzytkownik u
left join trofeum_uzytkownika tf
on u.ID_UZYTKOWNIKA = tf.ID_UZYTKOWNIKA
group by u.ID_UZYTKOWNIKA) zap;
 
-- ranking po ilosci banow
select pom.ID_UZYTKOWNIKA,
       ile,
       rank() over (order by ile) as pozycja
from (select UZYTKOWNIK.ID_UZYTKOWNIKA, count(ID_BANA) ile
from UZYTKOWNIK
left join ban
on UZYTKOWNIK.ID_UZYTKOWNIKA = BAN.ID_UZYTKOWNIKA
group by UZYTKOWNIK.ID_UZYTKOWNIKA) pom;
 
-- ranking adminow podleglych jakiemus po ilosci banow
with pom as (
    select admin.ID_UZYTKOWNIKA, ID_ZWIERZCHNIKA, count(id_bana) ile
    from admin
    left join ban
    on ban.ID_ADMINA = admin.ID_UZYTKOWNIKA
    group by admin.ID_UZYTKOWNIKA, admin.ID_ZWIERZCHNIKA
)
select ID_UZYTKOWNIKA,
       ile,
       rank() over (order by ile desc) pozycja
from (select *
from pom
start with ID_UZYTKOWNIKA = 6
connect by prior ID_UZYTKOWNIKA = ID_ZWIERZCHNIKA) pom2;
 
-- wypisywanie podleglych adminow
with pom(ID_UZYTKOWNIKA) as (
    select ID_UZYTKOWNIKA from ADMIN where ID_UZYTKOWNIKA = 6
    union all
    select ADMIN.ID_UZYTKOWNIKA from ADMIN, pom where ADMIN.ID_ZWIERZCHNIKA = pom.ID_UZYTKOWNIKA
)
select * from pom;
 
-- ponizsze zapytania sa przykladowe, beda pomocne przy budowaniu
-- bardziej skomplikowanych zapytan okreslonych w warstwie aplikacji
 
select id
from (select case
    when ID_UZYTKOWNIKA = 2 then ID_ZNAJOMEGO
    when ID_ZNAJOMEGO = 2 then ID_UZYTKOWNIKA
end id
from ZNAJOMOSC) pom
where id is not null;
 
-- niezbanowani uzytkownicy
select distinct ID_UZYTKOWNIKA
from ban
where DATA_WYGASNIECIA > current_timestamp;
 
-- wieksza ranga
select ID_UZYTKOWNIKA
from UZYTKOWNIK
where NR_POZIOMU_UZYTKOWNIKA > 1;
 
-- wieksza rowna
select ID_UZYTKOWNIKA
from UZYTKOWNIK
where NR_POZIOMU_UZYTKOWNIKA >= 3 and ID_UZYTKOWNIKA != 7;

-- zapytanie o przebieg rozgrywki
select *
from ZAPIS_ROZGRYWKI
where ID_ROZGRYWKI = 1;