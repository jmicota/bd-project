--BD PROJEKT MODEL LOGICZNY--
--PROCEDURY--

--Justyna Micota 418427--
--Paweł Sarzyński 418407--

-- dodawanie nowego użytkownika
-- banowanie użytkownika
-- odbanowywanie użytkownika
-- sprawdzanie czy użytkownik ma nałożony ban
-- dodawanie informacji dot. pojedynczej rozgrywki
-- dodawanie znajomego
-- usuwanie znajomego
-- dodawanie admina
-- usuwanie admina
-- dodawanie dotacji
-- przyznawanie trofeów

create or replace procedure dodaj_uzytk(log varchar2, haslo number,
                                        poz number, im VARCHAR2, 
                                        nazw VARCHAR2, datur DATE) is
BEGIN
    insert into uzytkownik(login, hash_hasla, nr_poziomu_uzytkownika, imie, nazwisko, data_urodzenia)
    values (log, haslo, poz, im, nazw, datur);
END;
/

create or replace procedure zbanuj(uzytk number, admin number, ile_dni number) IS
BEGIN
    insert into ban(id_uzytkownika, id_admina, liczba_dni)
    values (uzytk, admin, ile_dni);
END;
/

create or replace procedure odbanuj(uzytk number) IS
BEGIN
    UPDATE ban SET data_wygasniecia = (select current_timestamp from dual) 
    where id_uzytkownika = uzytk and 
          ban.data_wygasniecia > (select current_timestamp from dual);
END;
/

create or replace function czyzbanowany(uzytk number) return number IS
    retval number;
BEGIN
    SELECT count(*) into retval from ban 
    where ban.id_uzytkownika = uzytk and 
          ban.data_wygasniecia > (select current_timestamp from dual);
    return retval;
END;
/

create or replace function ret_dodaj_rozgrywke(gra number, tryb number) return number IS
    retval number;
BEGIN
    insert into rozgrywka(id_gry, id_trybu) values (gra, tryb);
    select MAX(id_rozgrywki) into retval from rozgrywka;
    return retval;
END;
/

create or replace procedure dodaj_rozgrywke(gra number, tryb number) is
BEGIN
    insert into rozgrywka(id_gry, id_trybu) values (gra, tryb);
END;
/

create or replace procedure dodaj_wynik(rozgrywka number, uzytk number, poz number) IS
BEGIN
    insert into wynik(id_rozgrywki, id_uzytkownika, pozycja)
    values (rozgrywka, uzytk, poz);
END;
/

create or replace procedure dodaj_ruch_rozgr(rozgrywka number, uzytk number, ruch varchar2) IS
BEGIN
    insert into zapis_rozgrywki(id_rozgrywki, id_uzytkownika, ruch)
    values (rozgrywka, uzytk, ruch);
END;
/

create or replace procedure dodaj_admina(uzytk number, zwierzchnik number, ranga number) IS
BEGIN
    insert into admin(id_uzytkownika, id_zwierzchnika, ranga_admina)
    values (uzytk, zwierzchnik, ranga);
END;
/

create or replace procedure usun_admina(uzytk number) IS
BEGIN
    delete from admin where id_uzytkownika = uzytk;
END;
/

create or replace procedure dodaj_dotacje(uzytk number, ile number) IS
BEGIN
    insert into dotacja(id_uzytkownika, kwota)
    values (uzytk, ile);
END;
/

create or replace procedure dodaj_znajomego(uzytk1 number, uzytk2 number) IS
BEGIN
    IF (uzytk1 > uzytk2) THEN
        insert into znajomosc(id_uzytkownika, id_znajomego)
        values (uzytk2, uzytk1);
    ELSE 
        insert into znajomosc(id_uzytkownika, id_znajomego)
        values (uzytk1, uzytk2);
    END IF;
END;
/

create or replace procedure usun_znajomego(uzytk1 number, uzytk2 number) IS
BEGIN
    IF (uzytk1 > uzytk2) THEN
        delete from znajomosc where znajomosc.id_uzytkownika = uzytk2 and znajomosc.id_znajomego = uzytk1;
    ELSE
        delete from znajomosc where znajomosc.id_uzytkownika = uzytk1 and znajomosc.id_znajomego = uzytk2;
    END IF;
END;
/

create or replace procedure przyznaj_trofeum(uzytk number, trofeum number) IS
BEGIN
    insert into trofeum_uzytkownika(id_uzytkownika, id_trofeum)
    values (uzytk, trofeum);
END;
/