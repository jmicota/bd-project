<?php
include 'pasek.php';
include 'zapytania.php';

zrob_poczatkowe_rzeczy();

$moje_id = daj_uzytkownika($_SESSION['login']);
$GLOBALS['kogo_moge_zbanowac'] = lista_kogo_moge_zbanowac($moje_id);

if (isset($_POST['dodaj'])) {
    $drugie_id = $_POST['dodaj'];
    dodaj_znajomosc($moje_id, $drugie_id);
} else if (isset($_POST['usun'])) {
    $drugie_id = $_POST['usun'];
    usun_znajomosc($moje_id, $drugie_id);
}

if (isset($_POST['ile_dni']) and isset($_POST['kogo'])) {
    $ile_dni = $_POST['ile_dni'];
    $kogo = $_POST['kogo'];
    zbanuj($moje_id, $kogo, $ile_dni);
}

$GLOBALS['uzytkownicy'] = daj_uzytkownikow();
$GLOBALS['znajomi'] = daj_liste_znajomych($_SESSION['login']);


function lista_kogo_moge_zbanowac($id) {
    $lista = array();
    $wynik = kogo_moge_zbanowac($id);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $id_uzyt = $wiersz['ID_UZYTKOWNIKA'];
        array_push($lista, $id_uzyt);
    }
    return $lista;
}

function przycisk_dodaj($id) {
    $html = '
        <div class="flexCol">
        <form action="info.html" method="post">
            <p><a href="info.html"><button name="dodaj" value="' . $id .'" class="przycisk-dodaj">
                Dodaj znajomego
            </button></a></p>        
        </form>
        </div>    
    ';
    return $html;
}

function przycisk_usun($id) {
    $html = '
        <div class="flexCol">
        <form action="info.html" method="post">
            <p><a href="info.html"><button name="usun" value ="' . $id . '" class="przycisk-usun">
                Usuń znajomego
            </button></a></p>        
        </form>
        </div>    
    ';
    return $html;
}

function czy_znajomy($id) {
    $znajomi = $GLOBALS['znajomi'];
    for ($i = 0; $i < count($znajomi); $i++)
        if ($id == $znajomi[$i])
            return true;
    return false;
}

function wybierz_przycisk($id) {
    if (!zalogowany())
        return '';
    if (czy_znajomy($id))
        return przycisk_usun($id);
    return przycisk_dodaj($id);
}

function czy_moge_zbanowac($id) {
    $lista = $GLOBALS['kogo_moge_zbanowac'];
    for ($i = 0; $i < count($lista); $i++)
        if ($id == $lista[$i])
            return true;
    return false;
}

function zrob_ban_przycisk($id) {
    if (!czy_moge_zbanowac($id))
        return '';
    $html = '
        <div class="flexCol">
            <form method="post" action="info.html" input style="display:inline;float:right;font-size:8px;" class="w3-margin-top">
                <label>
                    <input type="text" name="ile_dni">
                </label>
                <button name="kogo" value="' . $id . '"class="button-ban">
                    Zbanuj
                </button>
            </form>
        </div>    
    ';
    return $html;
}

function stworz_tabelke_trofeow($wynik) {
    $html = '
      <table>
          <tr>
            <th> Trofeum </th>
            <th> Gra </th>
            <th> Tryb </th>
            <th> Wymagany poziom </th>
            <th> Ile zwycięstw </th>
            <th> Jaki procent wygranych </th>
          </tr>
    ';
    while (($wiersz = oci_fetch_array($wynik))) {
        $html .= '<tr>';
        for ($i = 0; $i < 6; $i++)
            $html .= '<th>' . $wiersz[$i] . '</th>';
        $html .= '</tr>';
    }
    $html .= '</table>';
    return $html;
}

function pokaz_trofea() {
    $wynik = wyciagnij_info_o_trofeach();
    echo stworz_tabelke_trofeow($wynik);
}

function stworz_ranking_admina($id) {
    $wynik = daj_ranking_adminow($id);
    return stworz_tabelke($wynik, 'Bany');
}

function stworz_podleglych_adminow($id) {
    $wynik = daj_liste_podleglych_adminow($id);
    $html = '';
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $login = $wiersz['LOGIN'];
        $html .= '<p>' . $login .'</p>';
    }
    return $html;
}

function pokaz_dane_admina($id, $login) {
    $html = '
            <div class="flexWrap">

                <div class="flexCol">
                    <div class="w3-container w3-round w3-theme-pink">
                        <div class="w3-center"> ' .
                            $login . '
                        </div>
                    </div>
                </div>

                <div class="flexCol">
                    <div class="w3-container w3-round w3-white"> ' .
                        stworz_ranking_admina($id) . '
                    </div>
                </div>

                <div class="flexCol">
                    <div class="w3-container w3-round w3-white"> ' .
                        stworz_podleglych_adminow($id) . '
                    </div>
                </div>

            </div>    
    ';
    echo $html;
}

function pokaz_wszystkich_adminow() {
    $wynik = daj_wszystkich_adminow();
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $login = $wiersz['LOGIN'];
        $id = $wiersz['ID_UZYTKOWNIKA'];
        pokaz_dane_admina($id, $login);
    }
}

function stworz_uzytkownika($id, $login) {
    $html = '
            <div class="flexWrap">
                <div class="flexCol">
                    <div class="w3-container w3-round">
                        <div class="w3-center w3-margin-top">' .
                            $login . '
                        </div>
                    </div>
                </div>' .
            wybierz_przycisk($id) . ' ' .
            zrob_ban_przycisk($id)  . '
            </div>    
    ';
    echo $html;
}

function pokaz_dotacje() {
    echo stworz_tabelke_dotacji();
}

function stworz_wszystkich_uzytkownikow() {
    $uzytkownicy = $GLOBALS['uzytkownicy'];
    for ($i = 0; $i < count($uzytkownicy); $i++)
        if ($_SESSION['login'] != $uzytkownicy[$i][1])
            stworz_uzytkownika($uzytkownicy[$i][0], $uzytkownicy[$i][1]);
}

?>
