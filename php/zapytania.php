<?php
define('zapytanie_o_wygrane', '
        select *
        from (select zap.LOGIN,
               ile,
               rank() over (order by ile desc) as pozycja
        from (select u.login, count(distinct ID_ROZGRYWKI) ile
        from wszyscy u
        left join jakie_rozgrywki
        on u.ID_UZYTKOWNIKA = jakie_rozgrywki.id_uzytkownika
        and POZYCJA = 1
        group by u.ID_UZYTKOWNIKA, u.login) zap
        order by pozycja)
        where rownum <= 10
');

define('zapytanie_o_rozegrane', '
        select *
        from (select zap.LOGIN,
               ile,
               rank() over (order by ile desc) as pozycja
        from (select u.ID_UZYTKOWNIKA, u.LOGIN, count(distinct jakie_rozgrywki.ID_ROZGRYWKI) ile
        from wszyscy u
        left join jakie_rozgrywki
        on u.ID_UZYTKOWNIKA = jakie_rozgrywki.id_uzytkownika
        group by u.ID_UZYTKOWNIKA, u.LOGIN) zap
        order by pozycja)
        where rownum <= 10
');

define('zapytanie_o_procent', '
        select zap.LOGIN login,
           zap.proc ile,
           rank() over (order by proc desc) as pozycja
        from (select wygrane.LOGIN, case
            when rozegrane.ile = 0 then 0
            else wygrane.ile / rozegrane.ile * 100
        end proc
        from rozegrane
        left join wygrane
        on rozegrane.ID_UZYTKOWNIKA = wygrane.ID_UZYTKOWNIKA) zap
');

define('zapytanie_o_trofea', '
        select * from
        (select zap.LOGIN as login,
               ile,
               rank() over (order by ile desc) as pozycja
        from (select u.LOGIN, count(ID_TROFEUM) ile
        from wszyscy u
        left join jakie_trofea tf
        on u.ID_UZYTKOWNIKA = tf.ID_UZYTKOWNIKA
        group by u.ID_UZYTKOWNIKA, u.LOGIN) zap order by pozycja)
        where rownum <= 10
');

define('wszyscy_dla_ogolnego', '
    wszyscy as (
        select id_uzytkownika, login from uzytkownik
    )
');

define('wszyscy_dla_znajomych', '
   wszyscy as (
        select LOGIN, ID_UZYTKOWNIKA
        from znajomi
        join UZYTKOWNIK
        on znajomi.id = ID_UZYTKOWNIKA
    )
');

define('rozegrane', '
    rozegrane as (
        select u.ID_UZYTKOWNIKA, u.login, count(distinct ID_ROZGRYWKI) ile
        from wszyscy u
        left join wynik
        on u.ID_UZYTKOWNIKA = wynik.id_uzytkownika
        group by u.ID_UZYTKOWNIKA, u.LOGIN  
    )
');

define('wygrane', '
    wygrane as (
        select u.ID_UZYTKOWNIKA, u.login, count(distinct ID_ROZGRYWKI) ile
        from wszyscy u
        left join jakie_rozgrywki
        on u.ID_UZYTKOWNIKA = jakie_rozgrywki.id_uzytkownika
        and POZYCJA = 1
        group by u.ID_UZYTKOWNIKA, u.LOGIN   
    )
');

function alert($msg) {
    echo "<script type='text/javascript'>console.log('$msg');</script>";
}

function daj_uzytkownika($login) {
    $zap = 'select id_uzytkownika from uzytkownik where login = \'' . $login . '\'';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        return $wiersz['ID_UZYTKOWNIKA'];
    }
    return null;
}

function daj_calego_uzytkownika($login) {
    $zap = 'select * from uzytkownik where login = \'' . $login . '\'';
    return zwroc_wynik($zap);
}

function nazwa_poziomu($nr) {
    $zap = 'select nazwa_poziomu from poziom_uzytkownika where nr_poziomu = '  . $nr;
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        return $wiersz['NAZWA_POZIOMU'];
    }
    return null;
}

function daj_gre($nazwa) {
    $zap = 'select GRA.ID_GRY from GRA where NAZWA_GRY = \'' . $nazwa . '\'';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        return $wiersz['ID_GRY'];
    }
    return null;
}

function daj_tryb($nazwa) {
    $zap = 'select id_trybu from tryb_gry where nazwa_trybu = \'' . $nazwa . '\'';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        return $wiersz['ID_TRYBU'];
    }
    return null;
}

function daj_znajomych($nr, $czy_dodac) {
    $zap = '
        with znajomi as (
            select id
            from (select case
                when ID_UZYTKOWNIKA = ' . $nr . ' then ID_ZNAJOMEGO
                when ID_ZNAJOMEGO = ' . $nr . ' then ID_UZYTKOWNIKA
            end id
            from ZNAJOMOSC) pom
            where id is not null 
    ';
    if ($czy_dodac)
        $zap .= 'union select ' . $nr . ' from dual';
    $zap .= ')';
    return $zap;
}

function daj_graczy_danego_typu($nazwa_typu) {
    $zap = '
        with znajomi as (
            select UZYTKOWNIK.ID_UZYTKOWNIKA id
            from UZYTKOWNIK
            join POZIOM_UZYTKOWNIKA
            on UZYTKOWNIK.nr_poziomu_uzytkownika = POZIOM_UZYTKOWNIKA.nr_poziomu
            where NAZWA_POZIOMU = \'' . $nazwa_typu . '\'
        )
    ';
    return $zap;
}

function stworz_okienko_bana($login) {
    $wynik = daj_informacje_o_banie($login);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $komunikat = '
            <p>Zostałeś zbanowany przez ' . $wiersz['LOGIN'] . '!! :(</p>
            <p>Ban wygaśnie ' . $wiersz['DATA_WYGASNIECIA'] . '. Więcej info w zakładce \'FAQ\'!</p>        
        ';
        $html .= stworz_okienko('w3-theme-alert', $komunikat);
    }
    return $html;
}

function daj_loginy_znajomych($nr) {
    $zap = daj_znajomych($nr, false) . '
        select LOGIN
        from znajomi
        join UZYTKOWNIK
        on znajomi.id = ID_UZYTKOWNIKA  
    ';
    return zwroc_wynik($zap);
}

function daj_liste_znajomych($login) {
    $nr = daj_uzytkownika($login);
    $zap = daj_znajomych($nr, false) . ' select * from znajomi';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    $tablica = array();
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $id = $wiersz['ID'];
        array_push($tablica, $id);
    }
    return $tablica;
}

function dodaj_znajomosc($id1, $id2) {
    $zap = 'CALL dodaj_znajomego(' . $id1 . ', ' . $id2 . ')';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
}

function usun_znajomosc($id1, $id2) {
    $zap = 'CALL usun_znajomego(' . $id1 . ', ' . $id2 . ')';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
}

function czy_jestem_adminem($id) {
    $zap = 'select id_uzytkownika from admin where id_uzytkownika = \''. $id . '\'';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        return true;
    }
    return false;
}

function wyciagnij_info_o_trofeach() {
    $zap = '
        select NAZWA_TROFEUM, NAZWA_GRY, NAZWA_TRYBU, NAZWA_POZIOMU, WYM_LICZBA_ZWYCIESTW, WYM_PROC_ZWYCIESTW
        from TROFEUM
        join gra on TROFEUM.ID_GRY = gra.ID_GRY
        join POZIOM_UZYTKOWNIKA on TROFEUM.WYM_POZIOM = POZIOM_UZYTKOWNIKA.NR_POZIOMU
        join TRYB_GRY on TROFEUM.ID_TRYBU = TRYB_GRY.ID_TRYBU    
    ';
    return zwroc_wynik($zap);
}

function daj_wszystkich_adminow() {
    $zap = '
        select ADMIN.ID_UZYTKOWNIKA, UZYTKOWNIK.LOGIN
        from ADMIN
        join UZYTKOWNIK
        on ADMIN.ID_UZYTKOWNIKA = UZYTKOWNIK.ID_UZYTKOWNIKA
    ';
    return zwroc_wynik($zap);
}

function daj_moje_rozgrywki($id) {
    $zap ='
        select distinct WYNIK.ID_ROZGRYWKI, NAZWA_GRY, NAZWA_TRYBU
        from WYNIK
        join ROZGRYWKA on WYNIK.ID_ROZGRYWKI = ROZGRYWKA.ID_ROZGRYWKI
        join GRA on ROZGRYWKA.ID_GRY = Gra.ID_GRY
        join TRYB_GRY on ROZGRYWKA.ID_TRYBU = TRYB_GRY.ID_TRYBU
        where ID_UZYTKOWNIKA = ' . $id . '
        order by ID_ROZGRYWKI desc
    ';
    return zwroc_wynik($zap);
}

function daj_ranking_adminow($id) {
    $zap = '
        with pom as (
            select admin.ID_UZYTKOWNIKA, ID_ZWIERZCHNIKA, count(id_bana) ile
            from admin
            left join ban
            on ban.ID_ADMINA = admin.ID_UZYTKOWNIKA
            group by admin.ID_UZYTKOWNIKA, admin.ID_ZWIERZCHNIKA
        )
        select LOGIN,
               ile,
               rank() over (order by ile desc) pozycja
        from (select *
        from pom
        start with ID_UZYTKOWNIKA =  ' . $id .  '
        connect by prior ID_UZYTKOWNIKA = ID_ZWIERZCHNIKA) pom2
        join UZYTKOWNIK
        on UZYTKOWNIK.ID_UZYTKOWNIKA = pom2.ID_UZYTKOWNIKA
        where ile > 0
    ';
    return zwroc_wynik($zap);
}

function daj_liste_podleglych_adminow($id) {
    $zap = '
        with pom(ID_UZYTKOWNIKA) as (
            select ID_UZYTKOWNIKA from ADMIN where ID_UZYTKOWNIKA = ' . $id . '
            union all
            select ADMIN.ID_UZYTKOWNIKA from ADMIN, pom where ADMIN.ID_ZWIERZCHNIKA = pom.ID_UZYTKOWNIKA
        )
        select LOGIN
        from pom
        join UZYTKOWNIK
        on pom.ID_UZYTKOWNIKA = UZYTKOWNIK.ID_UZYTKOWNIKA    
    ';
    return zwroc_wynik($zap);
}

function zbanuj($admin, $kogo, $ile_dni) {
    $zap = 'CALL zbanuj(' . $kogo . ', ' . $admin . ', ' . $ile_dni . ')';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
}

function daj_historie_rozgrywki($id) {
    $zap ='
        select RUCH
        from ZAPIS_ROZGRYWKI
        where id_rozgrywki = ' . $id .'
        order by ID_RUCHU
    ';
    return zwroc_wynik($zap);
}

function daj_uzytkownikow() {
    $zap = 'select id_uzytkownika, login from uzytkownik';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    $tablica = array();
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $id = $wiersz['ID_UZYTKOWNIKA'];
        $login = $wiersz['LOGIN'];
        array_push($tablica, array($id, $login));
    }
    return $tablica;
}

function daj_trofea_uzytkownika($id) {
    $zap = '
        select NAZWA_TROFEUM
        from TROFEUM_UZYTKOWNIKA
        join TROFEUM T on T.ID_TROFEUM = TROFEUM_UZYTKOWNIKA.ID_TROFEUM
        where TROFEUM_UZYTKOWNIKA.ID_UZYTKOWNIKA = ' . $id . '   
    ';
    return zwroc_wynik($zap);
}

function stworz_okienko($styl, $komunikat) {
    $okienko = '
      <div class="w3-container w3-display-container w3-round ' . $styl . ' w3-border w3-theme-border w3-margin-bottom w3-hide-small">
        <span onclick="this.parentElement.style.display=\'none\'" class="w3-button w3-theme-l3 w3-display-topright">
          <i class="fa fa-remove"></i>
        </span>' .
        $komunikat .
        '</div>
    ';
    return $okienko;
}

function daj_informacje_o_banie($login) {
    $id = daj_uzytkownika($login);
    $zap = '
        select UZYTKOWNIK.LOGIN, DATA_WYGASNIECIA
        from (select ID_ADMINA, DATA_WYGASNIECIA
        from ban
        where ID_UZYTKOWNIKA = ' . $id . ' and DATA_WYGASNIECIA > CURRENT_TIMESTAMP and rownum <= 1
        order by DATA_WYGASNIECIA desc) zap
        join UZYTKOWNIK
        on zap.ID_ADMINA = UZYTKOWNIK.ID_UZYTKOWNIKA
    ';
    return zwroc_wynik($zap);
}

function daj_ostatnie_trofea() {
    $zap = '
        select TROFEUM.NAZWA_TROFEUM
        from TROFEUM_UZYTKOWNIKA
        join TROFEUM
        on TROFEUM_UZYTKOWNIKA.ID_TROFEUM = TROFEUM.ID_TROFEUM
        where ROWNUM <= 10    
    ';
    return zwroc_wynik($zap);
}

function daj_dostepne_tryby($login) {
    $zap = '
        select NAZWA_TRYBU
        from tryb_gry
        join uzytkownik
        on uzytkownik.login = \'' . $login . '\'
        where uzytkownik.nr_poziomu_uzytkownika >= tryb_gry.wymagany_poziom
    ';
    return zwroc_wynik($zap);
}

function jakie_rozgrywki_znajomych($nr) {
    $zap = 'jakie_rozgrywki as (
                select *
                from WYNIK
                where WYNIK.ID_UZYTKOWNIKA in (select * from znajomi)
                or WYNIK.ID_UZYTKOWNIKA = ' . $nr . '
           )';
    return $zap;
}
function daj_nazwy_gier() {
    $zap = 'select nazwa_gry from gra';
    return zwroc_wynik($zap);
}

function daj_haslo_uzytkownika($login) {
    $zap = '
        select hash_hasla
        from uzytkownik
        where login = \'' . $login . '\'';
    alert('jestem1');
    $wynik = zwroc_wynik($zap);
    alert('jestem1');
    while ($wiersz = oci_fetch_assoc($wynik)) {
        alert('jestem2');
        return $wiersz['HASH_HASLA'];
    }
    alert('jestem3');
    return null;
}

function jakie_trofea_znajomych($nr) {
    $zap = '
        jakie_trofea as (
            select TROFEUM_UZYTKOWNIKA.ID_TROFEUM, TROFEUM_UZYTKOWNIKA.ID_UZYTKOWNIKA
            from TROFEUM_UZYTKOWNIKA
            join TROFEUM
            on TROFEUM_UZYTKOWNIKA.ID_TROFEUM = trofeum.ID_TROFEUM
            and TROFEUM_UZYTKOWNIKA.ID_UZYTKOWNIKA in (select * from znajomi)
    )';
    return $zap;
}

function jakie_rozgrywki_typu($nazwa_gry) {
    $nr_gry = daj_gre($nazwa_gry);
    $zap = 'jakie_rozgrywki as (
        select WYNIK.ID_ROZGRYWKI, WYNIK.ID_UZYTKOWNIKA, WYNIK.POZYCJA
        from WYNIK
        join ROZGRYWKA
        on WYNIK.ID_ROZGRYWKI = ROZGRYWKA.ID_ROZGRYWKI
        and ROZGRYWKA.ID_GRY = ' . $nr_gry . '
        and WYNIK.ID_UZYTKOWNIKA in (select * from znajomi)
    )';
    return $zap;
}

function kto_mnie_bnowal($id) {
    $zap = '
        select distinct admin.LOGIN
        from ban
        join UZYTKOWNIK admin
        on ban.ID_ADMINA = admin.ID_UZYTKOWNIKA
        where ban.ID_UZYTKOWNIKA = ' . $id . '
    ';
    return zwroc_wynik($zap);
}

function wyciagnij_dotujacych() {
    $zap = '
        select UZYTKOWNIK.LOGIN , sum(KWOTA) suma
        from UZYTKOWNIK
        join DOTACJA
        on UZYTKOWNIK.ID_UZYTKOWNIKA = DOTACJA.ID_UZYTKOWNIKA
        group by UZYTKOWNIK.ID_UZYTKOWNIKA, UZYTKOWNIK.LOGIN
        order by suma desc
    ';
    return zwroc_wynik($zap);
}

function stworz_tabelke_dotacji() {
    $wynik = wyciagnij_dotujacych();
    $html = '
        <table>
            <tr>
                <th>Login</th>
                <th>Suma</th>
            </tr>
    ';
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $ile = $wiersz['SUMA'];
        $login = $wiersz['LOGIN'];
        $html .= '<tr>
                    <th>' . $login . '</th>
                    <th>' . $ile . ' zł</th>
                  </tr>';
    }
    $html .= '</table>';
    return $html;
}

function czy_zbanowany($login) {
    $id = daj_uzytkownika($login);
    $zap = 'select czyzbanowany(' . $id . ') wynik from dual';
    $wynik = zwroc_wynik($zap);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $wyn = $wiersz['WYNIK'];
        if ($wyn == '1')
            return true;
    }
    return false;
}

function kogo_moge_zbanowac($id) {
    $zap = '
        select UZYTKOWNIK.ID_UZYTKOWNIKA
        from UZYTKOWNIK
        left join admin
        on UZYTKOWNIK.ID_UZYTKOWNIKA = admin.ID_UZYTKOWNIKA
        where nvl(ADMIN.RANGA_ADMINA, 0) < (select RANGA_ADMINA from ADMIN where ID_UZYTKOWNIKA = ' . $id . ')    
    ';
    return zwroc_wynik($zap);
}

function dodaj_koncowke_ogolne($zap, $nazwa_gry, $nazwa_trybu, $tabela) {
    if ($nazwa_gry != "") {
        $nr_gry = daj_gre($nazwa_gry);
        $zap .= ' and ' . $tabela . '.ID_GRY = ' . $nr_gry;
        if ($nazwa_trybu != "") {
            $nr_trybu = daj_tryb($nazwa_trybu);
            $zap .= ' and ' . $tabela . '.ID_TRYBU = ' . $nr_trybu;
        }
    }
    return $zap . ')';
}

function jakie_rozgrywki_ogolne($nazwa_gry, $nazwa_trybu) {
    $zap = 'with jakie_rozgrywki as (
        select WYNIK.ID_ROZGRYWKI, WYNIK.ID_UZYTKOWNIKA, WYNIK.POZYCJA
        from WYNIK
        join ROZGRYWKA
        on WYNIK.ID_ROZGRYWKI = ROZGRYWKA.ID_ROZGRYWKI
    ';
    return dodaj_koncowke_ogolne($zap, $nazwa_gry, $nazwa_trybu, 'ROZGRYWKA');
}

function jakie_trofea_ogolne($nazwa_gry, $nazwa_trybu) {
    $zap = '
        with jakie_trofea as (
            select TROFEUM_UZYTKOWNIKA.ID_TROFEUM, TROFEUM_UZYTKOWNIKA.ID_UZYTKOWNIKA
            from TROFEUM_UZYTKOWNIKA
            join TROFEUM
            on TROFEUM_UZYTKOWNIKA.ID_TROFEUM = trofeum.ID_TROFEUM
    ';
    return dodaj_koncowke_ogolne($zap, $nazwa_gry, $nazwa_trybu, 'TROFEUM');
}

function wygrane_znajomi($nr) {
    return daj_znajomych($nr, true) . ', ' . jakie_rozgrywki_znajomych($nr) . ', ' . wszyscy_dla_znajomych . zapytanie_o_wygrane;
}

function rozegrane_znajomi($nr) {
    return daj_znajomych($nr, true) . ', ' . jakie_rozgrywki_znajomych($nr) . ', ' . wszyscy_dla_znajomych . zapytanie_o_rozegrane;
}

function procent_znajomi($nr) {
    return daj_znajomych($nr, true) . ', ' . jakie_rozgrywki_znajomych($nr) . ', ' . wszyscy_dla_znajomych . ', ' . rozegrane . ', ' . wygrane .' ' . zapytanie_o_procent;
}

function trofea_znajomi($nr) {
    return  daj_znajomych($nr, true) . ', ' . jakie_trofea_znajomych($nr) . ', ' . wszyscy_dla_znajomych . zapytanie_o_trofea;
}

function wygrane_typu($nazwa_typu, $nazwa_gry) {
    return daj_graczy_danego_typu($nazwa_typu) . ', ' . jakie_rozgrywki_typu($nazwa_gry) . ', ' . wszyscy_dla_znajomych . zapytanie_o_wygrane;
}

function rezegrane_typu($nazwa_typu, $nazwa_gry) {
    return daj_graczy_danego_typu($nazwa_typu) . ', ' . jakie_rozgrywki_typu($nazwa_gry) . ', ' . wszyscy_dla_znajomych . zapytanie_o_rozegrane;
}

function procent_typu($nazwa_typu, $nazwa_gry) {
    return daj_graczy_danego_typu($nazwa_typu) . ', ' . jakie_rozgrywki_typu($nazwa_gry) . ', ' . wszyscy_dla_znajomych . ', ' . rozegrane . ', ' . wygrane .' ' . zapytanie_o_procent;
}

function wygrane_ogolne($nazwa_gry, $nazwa_trybu) {
    return jakie_rozgrywki_ogolne($nazwa_gry, $nazwa_trybu) . ', ' . wszyscy_dla_ogolnego . zapytanie_o_wygrane;
}

function rozegrane_ogolne($nazwa_gry, $nazwa_trybu) {
    return jakie_rozgrywki_ogolne($nazwa_gry, $nazwa_trybu) . ', ' . wszyscy_dla_ogolnego . zapytanie_o_rozegrane;
}

function procent_ogolne($nazwa_gry, $nazwa_trybu) {
    return jakie_rozgrywki_ogolne($nazwa_gry, $nazwa_trybu) . ', ' . wszyscy_dla_ogolnego . ', ' . rozegrane . ', ' . wygrane .' ' . zapytanie_o_procent;
}

function trofea_ogolne($nazwa_gry, $nazwa_trybu) {
    return jakie_trofea_ogolne($nazwa_gry, $nazwa_trybu) . ', ' . wszyscy_dla_ogolnego . zapytanie_o_trofea;
}

function zwroc_wynik($zap) {
    if (!$_SESSION['conn'])
        alert('puste polaczenie');
//    $_SESSION['conn'] = oci_connect('ps418407', 'x', '//labora.mimuw.edu.pl/LABS');
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    return $wynik;
}

function ranking_trofea_ogolne($nazwa_typu, $nazwa_gry) {
    $zap = trofea_ogolne($nazwa_typu, $nazwa_gry);
    return zwroc_wynik($zap);
}

function ranking_trofea_znajomi($id) {
    $zap = trofea_znajomi($id);
    return zwroc_wynik($zap);
}

function ranking_procent_typu($nazwa_typu, $nazwa_gry) {
    $zap = procent_typu($nazwa_typu, $nazwa_gry);
    return zwroc_wynik($zap);
}

function ranking_wygrane_typu($nazwa_typu, $nazwa_gry) {
    $zap = wygrane_typu($nazwa_typu, $nazwa_gry);
    return zwroc_wynik($zap);
}

function ranking_rozegrane_typu($nazwa_typu, $nazwa_gry) {
    $zap = rezegrane_typu($nazwa_typu, $nazwa_gry);
    return zwroc_wynik($zap);
}

function ranking_wygrane_ogolne($nazwa_gry, $nazwa_trybu) {
    $zap = wygrane_ogolne($nazwa_gry, $nazwa_trybu);
    return zwroc_wynik($zap);
}

function ranking_rozegrane_ogolne($nazwa_gry, $nazwa_trybu) {
    $zap = rozegrane_ogolne($nazwa_gry, $nazwa_trybu);
    return zwroc_wynik($zap);
}

function ranking_procent_ogolne($nazwa_gry, $nazwa_trybu) {
    $zap = procent_ogolne($nazwa_gry, $nazwa_trybu);
    return zwroc_wynik($zap);
}

function ranking_wygrane_znajomi($nr) {
    $zap = wygrane_znajomi($nr);
    return zwroc_wynik($zap);
}

function ranking_rozegrane_znajomi($nr) {
    $zap = rozegrane_znajomi($nr);
    return zwroc_wynik($zap);
}

function ranking_procent_znajomi($nr) {
    $zap = procent_znajomi($nr);
    return zwroc_wynik($zap);
}

function stworz_tabelke($wynik, $po_czym) {
    $zap = '
      <table>
      <tr> <th> Pozycja </th> <th>Login</th>  <th>'.  $po_czym .' </th> <tr>
    ';
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $poz = $wiersz['POZYCJA'];
        $login = $wiersz['LOGIN'];
        $ile = $wiersz['ILE'];
        $zap .= '<tr> <th>' . $poz   . '</th><th>' . $login. '</th><th>' . $ile .'</th> </tr>';
    }
    $zap .= '</table>';
    return $zap;
}

function daj_typy_graczy() {
    $zap = 'select nazwa_poziomu from poziom_uzytkownika';
    return zwroc_wynik($zap);
}

?>
