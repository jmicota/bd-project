<?php
include 'pasek.php';
include 'zapytania.php';

zrob_poczatkowe_rzeczy();

function wygeneruj_okienko_rankingu($tekst, $tabelka, $nazwa) {
    $html = '
        <div class="w3-container">
         <a id="' . $nazwa . '">
            <div class="w3-container w3-white w3-margin-top">
              <h4><div class="w3-center" >' . $tekst . '</div></h4>
            </div>
          </a>
          
          <div class="w3-col m3">
          <div class="w3-container w3-white">
          <div class="w3-container" style="display: flex;justify-content: center;">
            <img src="../obrazki/miecze.png" class="image" style="width:180px;height:170px;">
          </div>
          </div>
          </div>

          <div class="w3-col m6">
          <div class="w3-container w3-theme-pink">
      
          <div class="w3-container w3-theme-col w3-round w3-margin-bottom w3-center w3-margin-top" style="text-align:center;">
          <div class="w3-container" style="display: flex;justify-content: center;"> ' .
          $tabelka .
          '</div>
          </div>

          </div>
          </div>

          <div class="w3-col m3">
          <div class="w3-container w3-white">
          <div class="w3-container" style="display: flex;justify-content: center;">
            <img src="../obrazki/miecze.png" class="image" style="width:180px;height:170px;">
          </div>
          </div>
          </div>

        </div>
      <br>
      
    ';
    return $html;
}

function ranking_liczba_wygranych() {
    $wynik = ranking_wygrane_ogolne("", "");
    $tabelka = stworz_tabelke($wynik, 'Wygrane');
    echo wygeneruj_okienko_rankingu('LICZBA ZWYCIĘSTW: ', $tabelka, 'wygrane');
}

function ranking_liczba_rozegranych() {
    $wynik = ranking_rozegrane_ogolne("", "");
    $tabelka = stworz_tabelke($wynik, 'Rozegrane');
    echo wygeneruj_okienko_rankingu('LICZBA ROZEGRANYCH: ', $tabelka, 'rozegrane');
}

function ranking_procent_wygranych() {
    $wynik = ranking_procent_ogolne("", "");
    $tabelka = stworz_tabelke($wynik, 'Procent');
    echo wygeneruj_okienko_rankingu('PROCENT WYGRANYCH: ', $tabelka, 'procent');
}

function wyswietl_rankingi_ogolne() {
    ranking_liczba_wygranych();
    ranking_liczba_rozegranych();
    ranking_procent_wygranych();
}
?>
