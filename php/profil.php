<?php
include 'pasek.php';
include 'zapytania.php';

zrob_poczatkowe_rzeczy();

function wyswietl_dane_uzytkownika() {
    $login = $_SESSION['login'];
    $wynik = daj_calego_uzytkownika($login);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $GLOBALS['nr'] = $wiersz['ID_UZYTKOWNIKA'];
        $poziom = nazwa_poziomu($wiersz['NR_POZIOMU_UZYTKOWNIKA']);
        $imie = $wiersz['IMIE'];
        $nazwisko = $wiersz['NAZWISKO'];
        $data_urodzenia = $wiersz['DATA_URODZENIA'];
    }
    echo '<p>LOGIN: ' . $login . '</p>';
    echo '<p>POZIOM: ' . $poziom . '</p>';
    echo '<p>IMIE: ' . $imie . '</p>';
    echo '<p>NAZWISKO: ' . $nazwisko . '</p>';
    echo '<p>DATA UR: ' . $data_urodzenia . '</p>';
}

function pokaz_bana() {
    if (!zalogowany())
        return;
    $login = $_SESSION['login'];
    echo stworz_okienko_bana($login);
}

function wyswietl_znajomych() {
    $nr = $GLOBALS['nr'];
    $wynik = daj_loginy_znajomych($nr);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $znajomy = $wiersz['LOGIN'];
        echo '<p>' . $znajomy . '</p>';
    }
}

function ranking_liczba_wygranych() {
    $nr = $GLOBALS['nr'];
    $wynik = ranking_wygrane_znajomi($nr);
    echo stworz_tabelke($wynik, 'Wygrane');
}

function ranking_liczba_rozegranych() {
    $nr = $GLOBALS['nr'];
    $wynik = ranking_rozegrane_znajomi($nr);
    echo stworz_tabelke($wynik, 'Rozegrane');
}

function ranking_procent_wygranych() {
    $nr = $GLOBALS['nr'];
    $wynik = ranking_procent_znajomi($nr);
    echo stworz_tabelke($wynik, 'Procent');
}

function ranking_trofea() {
    $nr = $GLOBALS['nr'];
    $wynik = ranking_trofea_znajomi($nr);
    echo stworz_tabelke($wynik, 'Trofea');
}

function stworz_jedna_rozgrywke($id_rozgrywki, $nazwa_gry, $nazwa_trybu) {
    $html = '
        <div class="w3-container">
            <div class="w3-container">
                Gra: ' . $nazwa_gry . " Tryb: ". $nazwa_trybu .'
            </div>

            <div class="w3-center">
                <form action="powtorka.html?nazwa_gry=' . $nazwa_gry . '&nazwa_trybu=' . $nazwa_trybu . '" method="post">
                    <p><button name="rozgrywka" value="' . $id_rozgrywki .'" class="w3-button w3-theme-col w3-margin-left">
                        ZOBACZ POWTÓRKĘ
                    </button></p>                
                </form>
            </div>
            <hr style="border: 1px solid black;">
        </div>    
    ';
    return $html;
}

function pokaz_banujacych_adminow() {
    $nr = $GLOBALS['nr'];
    $wynik = kto_mnie_bnowal($nr);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $admin = $wiersz['LOGIN'];
        echo '<p>' . $admin . '</p>';
    }
}

function pokaz_dotacje() {
    echo stworz_tabelke_dotacji();
}

function pokaz_ostatnie_rozgrywki() {
    $nr = $GLOBALS['nr'];
    $wynik = daj_moje_rozgrywki($nr);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $id_rozgrywki = $wiersz['ID_ROZGRYWKI'];
        $nazwa_gry = $wiersz['NAZWA_GRY'];
        $nazwa_trybu = $wiersz['NAZWA_TRYBU'];
        echo stworz_jedna_rozgrywke($id_rozgrywki, $nazwa_gry, $nazwa_trybu);
    }
}

function wypisz_moje_trofea() {
    $nr = $GLOBALS['nr'];
    $wynik = daj_trofea_uzytkownika($nr);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        echo '<p>' . $wiersz['NAZWA_TROFEUM'] . '</p>';
    }
}

?>
