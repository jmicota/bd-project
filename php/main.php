<?php
include 'pasek.php';
include 'zapytania.php';

zrob_poczatkowe_rzeczy();

function stworz_link_gry($nazwa) {
    $okienko = '
        <div class="w3-half">
          <div class="container">
            <img src="../obrazki/' . $nazwa. '.jpg" class="image w3-margin-bottom" style="width:100%">
            <div class="middle">
              <div class="text"><p><a href=wszystkie_gry.html#' . $nazwa . '>
                <button class="w3-button">' . strtoupper($nazwa) . '</button></p>
              </div></a>
            </div>
          </div>
        </div>
    ';
    return $okienko;
}

function stworz_linki_gier() {
    $wynik = daj_nazwy_gier();
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $nazwa = $wiersz['NAZWA_GRY'];
        echo stworz_link_gry($nazwa);
    }
}

function wypisz_komunikat() {
    if (!puste_dane()) {
        if ($_SESSION['poprawne'])
            echo stworz_okienko('w3-zalogowany', '<p>Witaj użytkowniku ' . $_SESSION['login'] . '!</p>');
        else
            echo stworz_okienko('w3-theme-alert', '<p>Podane błędne hasło lub/i login.</p>');
    }
}

function pokaz_rejestracje() {
    if (isset($_SESSION['info_rejestracja'])) {
        $info = $_SESSION['info_rejestracja'];
        if ($info == 'haslo')
            echo stworz_okienko('w3-theme-alert', '<p>Hasła się nie zgadzają!</p>');
        else if ($info == 'blad') {
            $komunikat = '
                <p>Nie można stworzyć takiego użytkownika. </p>
                <p>Spróbuj zarejestrować się jeszcze raz!</p>
            ';
            echo stworz_okienko('w3-theme-alert', $komunikat);
        } else if ($info == 'ok') {
            $komunikat = '
                <p>Konto założone poprawnie!</p>
                <p>Możesz teraz się zalogować.</p>
            ';
            echo stworz_okienko('w3-zalogowany', $komunikat);
        }
        zamknij_sesje();
        polacz_do_bazy();
    }
}

function pokaz_bana() {
    if (!zalogowany())
        return;
    $login = $_SESSION['login'];
    echo stworz_okienko_bana($login);
}

function wypisz_trofea() {
    $wynik = daj_ostatnie_trofea();
    while ($wiersz = oci_fetch_assoc($wynik)) {
        echo '<p>' . $wiersz['NAZWA_TROFEUM'] . '</p>';
    }
}

function wypisz_adminow() {
    $wynik = daj_wszystkich_adminow();
    while ($wiersz = oci_fetch_assoc($wynik)) {
        echo '<p>' . $wiersz['LOGIN'] . '</p>';
    }
}

function wypisz_najlepszych() {
    $wynik = ranking_wygrane_ogolne("", "");
    echo stworz_tabelke($wynik, 'Wygrane');
}

function wypisz_dotacje() {
    echo stworz_tabelke_dotacji();
}

?>