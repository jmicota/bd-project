<?php
include 'pasek.php';
include 'zapytania.php';

zrob_poczatkowe_rzeczy();

$GLOBALS['zbanowany'] = true;
$GLOBALS['tryby'] = array();

if (!czy_zbanowany($_SESSION['login']))
    $GLOBALS['zbanowany'] = false;

wyznacz_dostepne_tryby();

function wyznacz_dostepne_tryby() {
    $wynik = daj_dostepne_tryby($_SESSION['login']);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $tryb = $wiersz['NAZWA_TRYBU'];
        array_push($GLOBALS['tryby'], $tryb);
        alert("wrzucam");
    }
}

function daj_opis($nazwa) {
    $zap = '
        select PELNY_OPIS
        from opis
        where ID_OPISU =
        (select ID_OPISU
        from GRA
        where NAZWA_GRY = \'' . $nazwa . '\')
    ';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    $tekst = "<p>Opis gry: ";
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $opis = $wiersz['PELNY_OPIS'];
        $tekst = $tekst . $opis . '</p>';
    }
    return $tekst;
}

function wygeneruj_ranking($nazwa) {
    $zap = wygrane_ogolne($nazwa, "");
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    $lista = array();
    while ($wiersz = oci_fetch_assoc($wynik)) {
        array_push($lista, array($wiersz['LOGIN'], $wiersz['ILE']));
    }
    return $lista;
}

function przygotuj_ranking($ranking) {
    $wynik = "";
    for ($i = 0; $i < count($ranking); $i++)
        $wynik = $wynik . '<tr> <th>' . ($i + 1)  . '<th>' . $ranking[$i][0] . '<th>' . $ranking[$i][1] .'</th>';
    return $wynik;
}

function statystyki_uzytkownikow() {
    $tekst = "";
    if (zalogowany()) {
        $tekst = '
            <p>Wybierz tryb gry!</p>
            <p>Jeśli brakuje przycisków GRAJ poniżej, prawdopodobnie Twój poziom jest zbyt niski aby mieć dostęp do niektórych trybów, albo zostałeś zbanowany!</p>
        ';
    } else {
        $tekst = '
            <p>Grać w gry mogą tylko zalogowani użytkownicy.</p>
        ';
    }
    return $tekst;
}

function znajdz_trofea($nazwa) {
    $zap = '
        select TROFEUM.NAZWA_TROFEUM
        from TROFEUM
        where ID_GRY = (select ID_GRY from GRA where NAZWA_GRY = \''. $nazwa . '\')    
    ';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    $tekst = "";
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $nazwa = $wiersz['NAZWA_GRY'];
        echo '<a href="#' . $nazwa . '">' . strtoupper($nazwa) . '</a>';
    }
}

function stworz_przycisk($nazwa_gry, $nazwa_trybu) {
    $html = '
        <p><a href="graj.html?nazwa_gry=' . $nazwa_gry  . '&nazwa_trybu=' . $nazwa_trybu . '"><button class="w3-button w3-margin-left button-pos w3-theme-pink"> 
            GRAJ (' . strtoupper($nazwa_trybu)  .')
        </button></a></p>    
    ';
    return $html;
}

function stworz_wszystkie_przyciski($nazwa_gry) {
    if ($GLOBALS['zbanowany'])
        return '';
    $tryby = $GLOBALS['tryby'];
    for ($i = 0; $i < count($tryby); $i++)
        $wynik .= stworz_przycisk($nazwa_gry, $tryby[$i]);
    return $wynik;
}

function pokaz_gre($nazwa_gry) {
    $ranking = wygeneruj_ranking($nazwa_gry);
    $okienko = '
        <a id="' . $nazwa_gry . '"></a>
        <div class="w3-container w3-content" style="max-width:1600px;margin-top:40px">
            <div class="my-page-name">
                <p><div class="w3-white w3-round w3-center">
                <H4>' . strtoupper($nazwa_gry) . '</H4>
            </div></p>
                <!-- LEFT COL -->
                <div class="w3-col m7">
                    <div class="w3-container w3-card w3-theme-col w3-round w3-margin-left w3-margin-right"><br>
                        <div class="w3-container">
                            <div class="game-img">
                                <img src="../obrazki/' . $nazwa_gry .'.jpg" class="image w3-margin-bottom" style="width:100%">
                            </div>' .
                            daj_opis($nazwa_gry) . '
                            <p>Najwięcej wygranych w tą grę ma gracz ' . $ranking[0][0] . ', gratulujemy
                            pierwszego miejsca!</p>
                        </div>

                        ' .statystyki_uzytkownikow() .
                        stworz_wszystkie_przyciski($nazwa_gry) . '
                        
                        <p><a href="ranking_gry.html?nazwa_gry=' . $nazwa_gry . '"><button class="w3-button w3-center w3-white w3-margin-left">
                            ZOBACZ RANKINGI
                        </button></a></p>
                    </div>
                </div>
            </div>
            <!-- RIGHT COLUMN -->
            <div class="w3-col m5">
                <div class="w3-round w3-theme-grey w3-center w3-margin-right">
                    <div class="container w3-theme-pink w3-center w3-round w3-margin-bottom"><h4>
                        Najwięcej zwycięstw mają:
                    </h4></div>
                    <div class="w3-container" style="display: flex;justify-content: center;">
                        <table>
                          <thead>
                          <tr> <th> Pozycja <th>Login <th> Wygrane '.
                            przygotuj_ranking($ranking) . '
                        </table>
                    </div>
                </div>
                <br>
                <!-- End Right Column -->
            </div>
        </div>
        <div class="w3-center w3-margin-left w3-margin-right w3-margin-top w3-round line"
             style="width:84%; margin-left:8% !important; margin-right:8% !important;"></div>
    ';
    return $okienko;
}

function zrob_linki_do_gier() {
//    $wynik = $GLOBALS['wynik'];
    $zap = 'select nazwa_gry from gra';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $nazwa = $wiersz['NAZWA_GRY'];
        echo '<a href="#' . $nazwa . '">' . strtoupper($nazwa) . '</a>';
    }
}

function pokaz_wszystkie_gry() {
    $zap = 'select nazwa_gry from gra';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    oci_execute($wynik);
    $GLOBALS['wynik'] = $wynik;
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $nazwa = $wiersz['NAZWA_GRY'];
        echo pokaz_gre($nazwa);
    }
}

?>