<?php

function polacz_do_bazy() {
    if (!$_SESSION['conn']) {
        $_SESSION['conn'] = oci_connect('ps418407', 'x', '//labora.mimuw.edu.pl/LABS');
        if (!$_SESSION['conn']) {
            alert('blad');
            $e = oci_error();
        }
    }
}

function zrob_poczatkowe_rzeczy() {
    session_start();
    if (isset($_POST['login']) and $_POST['haslo']) {
        $_SESSION['login'] = $_POST['login'];
        $_SESSION['haslo'] = $_POST['haslo'];

        $_SESSION['akcja'] = 'logowanie';
    } else if (czy_rejstracja_pelna()) {
        $_SESSION['login_rej'] = $_POST['login_rej'];
        $_SESSION['haslo_raz'] = $_POST['haslo_raz'];
        $_SESSION['haslo_dwa'] = $_POST['haslo_dwa'];
        $_SESSION['imie'] = $_POST['imie'];
        $_SESSION['nazwisko'] = $_POST['nazwisko'];
        $_SESSION['data_urodzenia'] = $_POST['data_urodzenia'];

        $_SESSION['akcja'] = 'rejestracja';
    }

    if (isset($_POST['wylogowano']))
        zamknij_sesje();

    polacz_do_bazy();

    if (isset($_SESSION['akcja'])) {
        $akcja = $_SESSION['akcja'];
        if ($akcja == 'logowanie') {
            ocen_dane_logowania();
        } else if ($akcja == 'rejestracja') {
            ocen_dane_rejestracji();
        }
    }
}

function pokaz_panel() {
    if (zalogowany()) {
        $panel = '
            <form action="main.html" method="post">
            <a href="profil.html" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white"><i class="fa fa-user"></i></a>
            <button name="wylogowano" value="1" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-theme-pink w3-hover-white">WYLOGUJ</button>  
            </form>      
        ';
    } else {
        $panel = '
            <button onclick="document.getElementById(\'id02\').style.display=\'block\'" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-theme-col w3-hover-white">REJESTRACJA</button>
            <button onclick="document.getElementById(\'id01\').style.display=\'block\'" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-theme-pink w3-hover-white">ZALOGUJ</button>        
        ';
    }
    return $panel;
}

function pokaz_panel_profil() {
  $panel = '
      <form action="main.html" method="post">
      <button name="wylogowano" value="1" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-theme-pink w3-hover-white">WYLOGUJ</button>  
      </form>      
  ';
  return $panel;
}

function zamknij_sesje() {
    session_unset();
    session_destroy();
}

function czy_rejstracja_pelna() {
    return isset($_POST['login_rej']) and isset($_POST['haslo_raz']) and
        isset($_POST['haslo_dwa']) and isset($_POST['imie']) and
        isset($_POST['nazwisko']) and isset($_POST['data_urodzenia']);
}

function puste_dane() {
    return !isset($_SESSION['login']) or !isset($_SESSION['haslo']);
}

function ocen_dane_rejestracji() {
    $login = $_SESSION['login_rej'];
    $haslo = $_SESSION['haslo_raz'];
    $haslo_powt = $_SESSION['haslo_dwa'];
    if ($haslo != $haslo_powt) {
        $_SESSION['info_rejestracja'] = 'haslo';
        return;
    }
    $imie = $_SESSION['imie'];
    $nazwisko = $_SESSION['nazwisko'];
    $data_urodzenia = $_SESSION['data_urodzenia'];
    $zap = 'CALL dodaj_uzytk(\'' . $login . '\', ' . $haslo . ', 1, \'' . $imie .'\', \'' . $nazwisko . '\', to_date(\'' . $data_urodzenia . '\', \'YYYY-MM-DD\'))';
    $wynik = oci_parse($_SESSION['conn'], $zap);
    if (oci_execute($wynik))
        $_SESSION['info_rejestracja'] = 'ok';
    else
        $_SESSION['info_rejestracja'] = 'blad';
}

function ocen_dane_logowania() {
    $login = $_SESSION['login'];
    $haslo = $_SESSION['haslo'];
    $_SESSION['poprawne'] = false;
    $pobrane_haslo = daj_haslo_uzytkownika($login);
    if ($pobrane_haslo == $haslo)
        $_SESSION['poprawne'] = true;
}

function zalogowany() {
    return !puste_dane() and $_SESSION['poprawne'];
}

function pokaz_reszte() {
  $reszta = '
  <div id="id01" class="w3-modal w3-round">
  <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:500px">

    <div class="w3-center w3-theme-d2"><br>
      <span onclick="document.getElementById(\'id01\').style.display=\'none\'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
    </div>

    <form class="w3-container w3-theme-d2" action="main.html" method="post">
      <div class="w3-section">
        <label><b>Login</b></label>
        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Wprowadź login" name="login" required>
        <label><b>Hasło</b></label>
        <input class="w3-input w3-border" type="password" placeholder="Wprowadź hasło" name="haslo" required>
        <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Zaloguj</button>
        <input class="w3-check w3-margin-top" type="checkbox" checked="checked"> Zapamiętaj mnie
      </div>
    </form>

    <div class="w3-container w3-theme-d2 w3-border-top w3-padding-16">
      <button onclick="document.getElementById(\'id01\').style.display=\'none\'" type="button" class="w3-button w3-red">Cofnij</button>
      <span class="w3-right w3-padding w3-hide-small">Zapomniałeś <a href="#">hasła?</a></span>
    </div>

  </div>
  </div>

  <div id="id02" class="w3-modal w3-round">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:500px">

      <div class="w3-center w3-theme-d2"><br>
        <span onclick="document.getElementById(\'id02\').style.display=\'none\'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
      </div>

      <form class="w3-container w3-theme-d2" action="main.html" method="post">
        <div class="w3-section">
          <label><b>Login</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Wprowadź login" name="login_rej" required>
          <label><b>Hasło</b></label>
          <input class="w3-input w3-border" type="password" placeholder="Wprowadź hasło" name="haslo_raz" required><br>
          <label><b>Powtórz hasło</b></label>
          <input class="w3-input w3-border" type="password" placeholder="Powtórz hasło" name="haslo_dwa" required><br>
          <label><b>Imię</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Podaj imię" name="imie" required>
          <label><b>Nazwisko</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Podaj nazwisko" name="nazwisko" required>
          <label><b>Data urodzenia</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="date" placeholder="" name="data_urodzenia" required>
          <button name="rejestracja" value="1" class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Utwórz konto</button>
        </div>
      </form>

      <div class="w3-container w3-theme-d2 w3-border-top w3-padding-16">
        <button onclick="document.getElementById(\'id02\').style.display=\'none\'" type="button" class="w3-button w3-red">Cofnij</button>
      </div>

    </div>
  </div>    
  ';
  return $reszta;
}


function pokaz_pasek_main() {
    $html = '
        <div class="w3-top">
          <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
            <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
            <a href="wszystkie_gry.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">WSZYSTKIE GRY</a>
            <a href="ranking_ogolny.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">RANKINGI</a>
            <a href="info.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">INFO</a>
            <a href="faq.html" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white">FAQ</a>
            ' . pokaz_panel() .'
          </div>
        </div>   
        
        <div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
        <a href="wszystkie_gry.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">WSZYSTKIE GRY</a>
        <a href="ranking_ogolny.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">RANKINGI</a>
        <a href="info.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">INFO</a>
        <a href="faq.html" class="w3-bar-item w3-button w3-right w3-padding-large w3-hover-white">FAQ</a>
        </div>
        
        '.pokaz_reszte();
    echo $html;
}

function pokaz_pasek_wszystkie_gry() {
  $html = '
  <div class="w3-top">
    <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
      <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
      <a href="main.html" class="w3-bar-item w3-button w3-padding-large w3-theme-pink w3-hover-white">STRONA GŁÓWNA</a>
      <a href="ranking_ogolny.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">RANKINGI</a>
      <a href="info.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">INFO</a>
      <a href="faq.html" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white">FAQ</a>
      ' . pokaz_panel() .'
    </div>
  </div>    
      
      <div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
      <a href="wszystkie_gry.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">WSZYSTKIE GRY</a>
        <a href="ranking_ogolny.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">RANKINGI</a>
        <a href="info.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">INFO</a>
        <a href="faq.html" class="w3-bar-item w3-button w3-right w3-padding-large w3-hover-white">FAQ</a>
      </div>
      
      '.pokaz_reszte();
  echo $html;
}

function pokaz_pasek_graj() {
  $html = '
      <div class="w3-top">
        <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
          <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
          <a href="main.html" class="w3-bar-item w3-button w3-padding-large w3-theme-pink w3-hover-white">STRONA GŁÓWNA</a>
          <a href="wszystkie_gry.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">WSZYSTKIE GRY</a>
          <a href="ranking_ogolny.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">RANKINGI</a>
          <a href="info.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">INFO</a>
          <a href="faq.html" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white">FAQ</a>
          ' . pokaz_panel() .'
        </div>
      </div>   
      
      <div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
      <a href="main.html" class="w3-bar-item w3-button w3-padding-large w3-theme-pink w3-hover-white">STRONA GŁÓWNA</a>
      <a href="wszystkie_gry.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">WSZYSTKIE GRY</a>
      <a href="ranking_ogolny.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">RANKINGI</a>
      <a href="info.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">INFO</a>
      <a href="faq.html" class="w3-bar-item w3-button w3-right w3-padding-large w3-hover-white">FAQ</a>
      </div>
      '.pokaz_reszte();

  echo $html;
}

function pokaz_pasek_info() {
  $html = '
      <div class="w3-top">
        <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
          <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
          <a href="main.html" class="w3-bar-item w3-button w3-padding-large w3-theme-pink w3-hover-white">STRONA GŁÓWNA</a>
          <a href="wszystkie_gry.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">WSZYSTKIE GRY</a>
          <a href="ranking_ogolny.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">RANKINGI</a>
          <a href="faq.html" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white">FAQ</a>
          ' . pokaz_panel() .'
        </div>
      </div>   
      
      <div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
      <a href="main.html" class="w3-bar-item w3-button w3-padding-large w3-theme-pink w3-hover-white">STRONA GŁÓWNA</a>
      <a href="wszystkie_gry.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">WSZYSTKIE GRY</a>
      <a href="ranking_ogolny.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">RANKINGI</a>
      <a href="faq.html" class="w3-bar-item w3-button w3-right w3-padding-large w3-hover-white">FAQ</a>
      </div>
      '.pokaz_reszte();

  echo $html;
}

function pokaz_pasek_rankingi() {
  $html = '
      <div class="w3-top">
        <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
          <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
          <a href="main.html" class="w3-bar-item w3-button w3-padding-large w3-theme-pink w3-hover-white">STRONA GŁÓWNA</a>
          <a href="wszystkie_gry.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">WSZYSTKIE GRY</a>
          <a href="info.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">INFO</a>
          <a href="faq.html" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white">FAQ</a>
          ' . pokaz_panel() .'
        </div>
      </div>   
      
      <div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
      <a href="main.html" class="w3-bar-item w3-button w3-padding-large w3-theme-pink w3-hover-white">STRONA GŁÓWNA</a>
      <a href="wszystkie_gry.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">WSZYSTKIE GRY</a>
      <a href="info.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">INFO</a>
      <a href="faq.html" class="w3-bar-item w3-button w3-right w3-padding-large w3-hover-white">FAQ</a>
      </div>
      '.pokaz_reszte();

  echo $html;
}

function pokaz_pasek_profil() {
  $html = '
      <div class="w3-top">
        <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
          <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
          <a href="main.html" class="w3-bar-item w3-button w3-padding-large w3-theme-pink w3-hover-white">STRONA GŁÓWNA</a>
          <a href="wszystkie_gry.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">WSZYSTKIE GRY</a>
          <a href="ranking_ogolny.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">RANKINGI</a>
          <a href="info.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">INFO</a>
          <a href="faq.html" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white">FAQ</a>
          ' . pokaz_panel_profil() .'
        </div>
      </div>   
      
      <div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
      <a href="main.html" class="w3-bar-item w3-button w3-padding-large w3-theme-pink w3-hover-white">STRONA GŁÓWNA</a>
      <a href="wszystkie_gry.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">WSZYSTKIE GRY</a>
      <a href="ranking_ogolny.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">RANKINGI</a>
      <a href="info.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">INFO</a>
      <a href="faq.html" class="w3-bar-item w3-button w3-right w3-padding-large w3-hover-white">FAQ</a>
      </div>
      '.pokaz_reszte();
  echo $html;
}

function pokaz_pasek() {
  $html = '
      <div class="w3-top">
        <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
          <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
          <a href="main.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">GŁÓWNA</a>
          <a href="wszystkie_gry.html" class="w3-bar-item w3-button w3-padding-large w3-hover-white">WSZYSTKIE GRY</a>
          <a href="ranking_ogolny.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">RANKINGI</a>
          <a href="info.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">INFO</a>
          <a href="faq.html" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white">FAQ</a>
          ' . pokaz_panel() .'
        </div>
      </div>    
      <div id="id01" class="w3-modal w3-round">
        <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:500px">
      
          <div class="w3-center w3-theme-d2"><br>
            <span onclick="document.getElementById(\'id01\').style.display=\'none\'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
          </div>
      
          <form class="w3-container w3-theme-d2" action="main.html" method="post">
            <div class="w3-section">
              <label><b>Login</b></label>
              <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Wprowadź login" name="login" required>
              <label><b>Hasło</b></label>
              <input class="w3-input w3-border" type="password" placeholder="Wprowadź hasło" name="haslo" required>
              <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Zaloguj</button>
              <input class="w3-check w3-margin-top" type="checkbox" checked="checked"> Zapamiętaj mnie
            </div>
          </form>
      
          <div class="w3-container w3-theme-d2 w3-border-top w3-padding-16">
            <button onclick="document.getElementById(\'id01\').style.display=\'none\'" type="button" class="w3-button w3-red">Cofnij</button>
            <span class="w3-right w3-padding w3-hide-small">Zapomniałeś <a href="#">hasła?</a></span>
          </div>
      
        </div>
      </div>
      
      <div id="id02" class="w3-modal w3-round">
        <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:500px">
      
          <div class="w3-center w3-theme-d2"><br>
            <span onclick="document.getElementById(\'id02\').style.display=\'none\'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
          </div>
      
          <form class="w3-container w3-theme-d2" action="main.html" method="post">
            <div class="w3-section">
              <label><b>Login</b></label>
              <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Wprowadź login" name="login_rej" required>
              <label><b>Hasło</b></label>
              <input class="w3-input w3-border" type="password" placeholder="Wprowadź hasło" name="haslo_raz" required><br>
              <label><b>Powtórz hasło</b></label>
              <input class="w3-input w3-border" type="password" placeholder="Powtórz hasło" name="haslo_dwa" required><br>
              <label><b>Imię</b></label>
              <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Podaj login" name="imie" required>
              <label><b>Nazwisko</b></label>
              <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Podaj nazwisko" name="nazwisko" required>
              <label><b>Data urodzenia</b></label>
              <input class="w3-input w3-border w3-margin-bottom" type="date" placeholder="" name="data_urodzenia" required>
              <button name="rejestracja" value="1" class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Utwórz konto</button>
            </div>
          </form>
      
          <div class="w3-container w3-theme-d2 w3-border-top w3-padding-16">
            <button onclick="document.getElementById(\'id02\').style.display=\'none\'" type="button" class="w3-button w3-red">Cofnij</button>
          </div>
      
        </div>
      </div>
  ';
  echo $html;
}