<?php
include 'pasek.php';
include 'zapytania.php';

zrob_poczatkowe_rzeczy();

if (isset($_GET['nazwa_gry']) and isset($_GET['nazwa_trybu'])) {
    $GLOBALS['nazwa_gry'] = $_GET['nazwa_gry'];
    $GLOBALS['nazwa_trybu'] = $_GET['nazwa_trybu'];
}

function w_co_gram() {
    $nazwa = strtoupper($GLOBALS['nazwa_gry']);
    $html = '<strong>GRASZ W ' . $nazwa . ' </strong>';
    echo $html;
}

function wyswietl_tlo() {
    echo '<img src="../obrazki/' . $GLOBALS['nazwa_gry']   .'.jpg" class="image-in-play w3-margin-bottom" style="width:100%">';
}

function ranking_wygrane_gry_trybu() {
    $nazwa_gry = $GLOBALS['nazwa_gry'];
    $nazwa_trybu = $GLOBALS['nazwa_trybu'];
    $wynik = ranking_wygrane_ogolne($nazwa_gry, $nazwa_trybu);
    echo stworz_tabelke($wynik, 'Wygrane');
}

function ranking_rozegrane_gry_trybu() {
    $nazwa_gry = $GLOBALS['nazwa_gry'];
    $nazwa_trybu = $GLOBALS['nazwa_trybu'];
    $wynik = ranking_rozegrane_ogolne($nazwa_gry, $nazwa_trybu);
    echo stworz_tabelke($wynik, 'Rozegrane');
}

function ranking_procent_gry_trybu() {
    $nazwa_gry = $GLOBALS['nazwa_gry'];
    $nazwa_trybu = $GLOBALS['nazwa_trybu'];
    $wynik = ranking_procent_ogolne($nazwa_gry, $nazwa_trybu);
    echo stworz_tabelke($wynik, 'Procent');
}

function ranking_trofea_gry_trybu() {
    $nazwa_gry = $GLOBALS['nazwa_gry'];
    $nazwa_trybu = $GLOBALS['nazwa_trybu'];
    $wynik = ranking_trofea_ogolne($nazwa_gry, $nazwa_trybu);
    echo stworz_tabelke($wynik, 'Trofea');
}

?>
