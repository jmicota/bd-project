<?php
include 'pasek.php';
include 'zapytania.php';

zrob_poczatkowe_rzeczy();

$GLOBALS['nazwa_gry'] = $_GET['nazwa_gry'];
$GLOBALS['tablica_typow'] = array();

function wypisz_linki_do_typow() {
    $wynik = daj_typy_graczy();
    while ($wiersz = oci_fetch_assoc($wynik)) {
        $nazwa = $wiersz['NAZWA_POZIOMU'];
        array_push($GLOBALS['tablica_typow'], $nazwa);
        echo '<a href="#'. $nazwa .'">' . strtoupper($nazwa) . '</a>';
    }
}

function tytul_strony($nazwa_gry) {
 $html = '
 <H1><div class="w3-center w3-white w3-round w3-margin-bottom" ><strong>NAJLEPSI W GRĘ '. strtoupper($nazwa_gry) .' </strong></div></H1>';
 return $html;
}

function stworz_okno_typu($nazwa_typu, $wygrane, $rozegrane, $procent) {
    $html = '
    <div class="w3-container w3-margin-bottom w3-card w3-round">
        <a id="' . $nazwa_typu . '">
            <div class="w3-container w3-white w3-round w3-margin-top">
                <h4><div class="w3-center" >' . strtoupper($nazwa_typu) . ':</div></h4>
            </div>
        </a>
        
        <div class="w3-col m3">
          <div class="w3-container w3-white">
          <div class="w3-container" style="display: flex;justify-content: center;">
            <img src="../obrazki/miecze.png" class="image" style="width:180px;height:170px;">
          </div>
          </div>
        </div>

        <div class="w3-col m6">
        <div class="w3-container w3-theme-pink">

        <div class="w3-container w3-margin-bottom w3-margin-top w3-theme-col w3-round w3-center" style="display: flex;justify-content: center;"><br>' .
        stworz_tabelke($wygrane, 'Wygrane') . '
        </div>
        
        <div class="w3-container w3-margin-bottom w3-theme-col-light w3-round w3-center" style="display: flex;justify-content: center;"><br>' .
        stworz_tabelke($rozegrane, 'Rozegrane') . '
        </div>
        
        <div class="w3-container w3-margin-bottom w3-theme-col w3-round w3-center" style="display: flex;justify-content: center;"><br>' .
        stworz_tabelke($procent, 'Procent') . '
        </div>

        </div>
        </div>

        <div class="w3-col m3">
          <div class="w3-container w3-white">
          <div class="w3-container" style="display: flex;justify-content: center;">
            <img src="../obrazki/miecze.png" class="image" style="width:180px;height:170px;">
          </div>
          </div>
        </div>
        
    </div>
    <br>
    ';
    return $html;
}

function pokaz_tytul_strony() {
    $nazwa_gry = $GLOBALS['nazwa_gry'];
    echo tytul_strony($nazwa_gry);
}

function pokaz_okno_typu($nazwa_typu) {
    $nazwa_gry = $GLOBALS['nazwa_gry'];
    $wygrane = ranking_wygrane_typu($nazwa_typu, $nazwa_gry);
    $rozegrane = ranking_rozegrane_typu($nazwa_typu, $nazwa_gry);
    $procent = ranking_procent_typu($nazwa_typu, $nazwa_gry);
    echo stworz_okno_typu($nazwa_typu, $wygrane, $rozegrane, $procent);
}

function pokaz_okno_wszystkich() {
    $nazwa_gry = $GLOBALS['nazwa_gry'];
    $wygrane = ranking_wygrane_ogolne($nazwa_gry, '');
    $rozegrane = ranking_rozegrane_ogolne($nazwa_gry, '');
    $procent = ranking_procent_ogolne($nazwa_gry, '');
    echo stworz_okno_typu('Wszyscy', $wygrane, $rozegrane, $procent);
}

function stworz_wszystkie_rankingi() {
    pokaz_okno_wszystkich();
    $tablica = $GLOBALS['tablica_typow'];
    for ($i = 0; $i < count($tablica); $i++) {
        $nazwa = $tablica[$i];
        pokaz_okno_typu($nazwa);
    }
}

?>